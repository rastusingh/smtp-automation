# SMTP automation script prerequisits

## Getting started

**SMTP_Automation.sh**

>> The bash script is meant for CentOS 7.

>> Please ensure that the server where you want to run the script has a clean OS installation.

>> The sending domain needs to be declared to the bash script. Please use a text editor and search for [#DOMAIN#] and Replace all with your sending domain (xyz.com for example).

>> The chosesn domain/subdomain's **A record** must be pointed to the server's IP (for hostname and domain configuration purposes).

>> The script should run with superuser / root.

>> Please run the command chmod +x SMTP_Automation.sh before executing the script

**autossl.sh**

>> The **autossl** script automatically installs and configures (Let's encrypt) TLS on PMTA server. 

>> The sending domain needs to be declared to the bash script. Please use a text editor and search for [#DOMAIN#] and Replace all with your sending domain (xyz.com for example).

>> It is important to first run **SMTP_Automation.sh** script, let the auto configuration and restart complete before running the **autossl.sh** script. 
