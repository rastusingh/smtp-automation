#!/bin/bash

echo "Configuring LetsEncrypt Cetificate : 
### <=========================================================================================> ###
"
rm -rf /etc/ssl/private/pmta.[#DOMAIN#].pem

certbot --apache --non-interactive --agree-tos --domains [#DOMAIN#] --email postmaster@[#DOMAIN#]

cat /etc/letsencrypt/live/[#DOMAIN#]/cert.pem /etc/letsencrypt/live/[#DOMAIN#]/privkey.pem > /etc/ssl/private/pmta.[#DOMAIN#].pem

systemctl restart pmta

rm -rf /etc/profile.d/autossl.sh