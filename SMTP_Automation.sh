#!/bin/bash

### <=================================================================================================> ###
### PmtaExpert.com Centos 7 Installer =========================== > ###
 
### <=================================================================================================> ###

rm -rf /root/install.sh

yum install -y mailx

yum install -y unzip

yum install -y wget

yum -y update

mkdir /root/autosmtp-scripts

mkdir -p /root/autosmtp-scripts/backup-local/.Originais

mkdir /autosmtp

echo "Please Wait Downloading All The Important Files Needed To Install Your Server ##This May Take A Few Minutes##"

sleep 2
wget -q -c https://pmtaexpert.com/pipedrivetest/rastu/files.zip
sleep 2
mv files.zip /autosmtp
sleep 2
unzip -q /autosmtp/files.zip -d /autosmtp
sleep 2
echo "
### <==========================================================================================> ###
### <=================== SCRIPT CREATED BY Rastu ============================>###
Step 1 ### ---> Preparing installation CentOS 7 X86_64 = DATA ================================> ###
### <==========================================================================================> ###
"

ConfIps=`ip a | grep 'inet ' | awk '{print $2}' | cut -f1 -d/ | grep -v ^127.[0-9] | grep -v ^10.[0-9] | grep -v ^192.168.[0-9] | grep -v ^172.16.[0-9]`
echo "$ConfIps" > /root/autosmtp-scripts/ips.info
Ips=`cat /root/autosmtp-scripts/ips.info`
IpPrinc=`head -1 /root/autosmtp-scripts/ips.info`
IpsQuant=`cat /root/autosmtp-scripts/ips.info | wc -l`

echo "nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
" > /etc/resolv.conf

rm -rf /etc/localtime
echo 'ZONE=America/Chicago
UTC=false
ARC=false' > /etc/sysconfig/clock
ln -s /usr/share/zoneinfo/America/Chicago /etc/localtime


useradd pmtadm 
echo pmtadm:pmtadm@123 | chpasswd
mkdir /home/pmtadm/websites
chmod 755 /home/pmtadm/ -R
chown pmtadm:pmtadm /home/pmtadm/ -R
useradd return -s /sbin/nologin
echo return:[#XTRAEMAILPASS#] | chpasswd
useradd fbl 
echo fbl:[#XTRAEMAILPASS#] | chpasswd
useradd abuse 
echo abuse:[#XTRAEMAILPASS#] | chpasswd
useradd reply
echo reply:[#XTRAEMAILPASS#] | chpasswd
useradd postmaster
echo postmaster:[#XTRAEMAILPASS#] | chpasswd

echo titanium101.[#DOMAIN#] > /proc/sys/kernel/hostname
echo [#DOMAIN#] > /root/autosmtp-scripts/domain.info
echo mta00 > /root/autosmtp-scripts/reversedns.info
echo [#SQLPASS#] > /root/autosmtp-scripts/sqlpass.info
echo pmtadm > /root/autosmtp-scripts/sendinguser.info
echo pmtadm@123 > /root/autosmtp-scripts/sendinguserpass.info
echo 3000 > /root/autosmtp-scripts/ipspeed.info
echo 500 > /root/autosmtp-scripts/esplimit.info

yum remove php* httpd* mysql* postfix dovecot logwatch selinux-policy -y

yum install epel-release -y
rpm -Uvh /autosmtp/rpmforge-release-0.5.3-1.el7.rf.x86_64.rpm
rpm -Uvh /autosmtp/remi-release-7.rpm
rpm -Uvh /autosmtp/mysql-community-release-el7-5.noarch.rpm


yum update -y

yum install yum-utils -y

yum-config-manager --enable remi-php72

yum install dhcp python-certbot-apache bc vim-enhanced mysql-server httpd mod_ssl MySQL-python php gd php-gd php-xml php-mbstring php-mysql php-imap postfix cyrus-sasl cyrus-sasl-devel cyrus-sasl-gssapi cyrus-sasl-md5 cyrus-sasl-plain dovecot zip unzip openssl-devel make gcc sendmail-devel python-setuptools python-devel htop mlocate subversion nmap telnet yum-utils bind bind-chroot bind-libs bind-utils caching-nameserver vixie-cron ftp screen ntp ntpdate rdate wput phpmyadmin squid -y

yum install opendkim -y

echo "
### <=========================================================================================> ###
Step 2 ### ---> Preconfigured Named/Dkim - CentOS 7 x86_64 = NAMED/DKIM =====================> ###
### <=========================================================================================> ###
"

mv /etc/named.conf /etc/named.conf-bkp

echo "options {
    directory		\"/var/named\";
    dump-file		\"/var/named/data/cache_dump.db\";
    pid-file		\"/var/run/named/named.pid\";
    statistics-file	\"/var/named/data/named_stats.txt\";
    version		\"get lost\";
    allow-transfer	{\"none\";};
    recursion		no;

};

include \"/etc/rndc.key\";

zone \"[#DOMAIN#]\" {type master; file \"/var/named/[#DOMAIN#].db\";};" > /etc/named.conf

ArqNamed=`echo [#DOMAIN#].db`
SerialNamed=`date +%Y%m%d%H%M%S`

echo '$TTL    38400' > $ArqNamed
echo "@       IN      SOA     ns1.[#DOMAIN#].   postmaster.[#DOMAIN#]. (" >> $ArqNamed
echo "                        2019080100  ;serial" >> $ArqNamed
echo "                        90M ; refresh" >> $ArqNamed
echo "                        15M ; retry" >> $ArqNamed
echo "                        14D ; expire" >> $ArqNamed
echo "                        3600 ; default_ttl" >> $ArqNamed
echo "                        )" >> $ArqNamed
echo " " >> $ArqNamed

echo "[#DOMAIN#].   14400   IN   NS   ns1.[#DOMAIN#]." >> $ArqNamed
echo "[#DOMAIN#].   14400   IN   NS   ns2.[#DOMAIN#]." >> $ArqNamed
echo " " >> $ArqNamed
	
if [ $IpsQuant == 1 ]
then 
	echo "ns1   14400   IN   A   $IpPrinc" >> $ArqNamed  
	echo "ns2   14400   IN   A   $IpPrinc" >> $ArqNamed
else 
	Quant=1
	for Ip in $Ips
	do
		echo "ns$Quant   14400   IN   A   $Ip" >> $ArqNamed  
			Quant=`expr $Quant + 1`
			if [ $Quant -gt 2 ]
		then
			break
			fi
	done
fi
echo " " >> $ArqNamed

echo "[#DOMAIN#].   14400   IN   A   $IpPrinc" >> $ArqNamed 
echo "@   3600   IN   A   $IpPrinc" >> $ArqNamed 
echo " " >> $ArqNamed

echo "mail  14400   IN   A   $IpPrinc" >> $ArqNamed  
echo " " >> $ArqNamed

echo "localhost   14400   IN   A   127.0.0.1" >> $ArqNamed  
echo " " >> $ArqNamed

echo "@    86400    IN    MX    10  mail.[#DOMAIN#]." >> $ArqNamed

echo " " >> $ArqNamed

echo "server   IN   A   $IpPrinc" >> $ArqNamed
echo "www   IN   A   $IpPrinc" >> $ArqNamed
echo " " >> $ArqNamed

	Quant=0
	for Ip in $Ips
	do
		echo "titan$Quant   IN   A   $Ip" >> $ArqNamed  
		
		Quant=`expr $Quant + 1`
	done
echo " " >> $ArqNamed

if [ $IpsQuant -gt 8 ]
then
	sed -i "/titan\0/d" $ArqNamed
fi

cat /root/autosmtp-scripts/ips.info |cut -f1-3 -d. > /tmp/ips.info
sort /tmp/ips.info | uniq > /tmp/spfconfig.info
sed -i 's/^/ip4:/' /tmp/spfconfig.info
sed -i 's/$/.0\/24 /' /tmp/spfconfig.info
sed -i ':a;$!N;s/\n//;ta;' /tmp/spfconfig.info

SpfConfig=`cat /tmp/spfconfig.info`			
echo "[#DOMAIN#].   IN   TXT   \"v=spf1 a mx $SpfConfig ?all\"

_adsp._domainkey.[#DOMAIN#].   IN   TXT   \"dkim=all\"

_domainkey.[#DOMAIN#].   IN   TXT   \"o=~; r=abuse@[#DOMAIN#]\"

_dmarc.[#DOMAIN#].   IN   TXT   \"v=DMARC1; p=reject; adkim=r; aspf=r; pct=100; ruf=mailto:abuse@[#DOMAIN#]; rua=mailto:abuse@[#DOMAIN#]\"


" >> $ArqNamed

opendkim-default-keygen
mv /etc/opendkim/keys/default.private /tmp/dkim-default
cat /etc/opendkim/keys/default.txt >> $ArqNamed

mv $ArqNamed /var/named/$ArqNamed
chown root:named /var/named/$ArqNamed

ArqNamedRFC=/var/named/chroot/etc/named.rfc1912.zones

echo " " >> $ArqNamedRFC
echo "zone \"[#DOMAIN#]\" {" >> $ArqNamedRFC
echo "  type master;" >> $ArqNamedRFC
echo "  file \"$ArqNamed\";" >> $ArqNamedRFC
echo "  allow-query { any; };" >> $ArqNamedRFC
echo "};" >> $ArqNamedRFC


echo "Test POSTFIX_DKIM"

### START


mv /etc/opendkim.conf /etc/opendkim.conf.orig

cat > /etc/opendkim.conf <<EOF
AutoRestart             Yes  
AutoRestartRate         10/1h  
LogWhy                  Yes  
Syslog                  Yes  
SyslogSuccess           Yes  
Mode                    sv  
Canonicalization        relaxed/simple  
ExternalIgnoreList      refile:/etc/opendkim/TrustedHosts  
InternalHosts           refile:/etc/opendkim/TrustedHosts  
KeyTable                refile:/etc/opendkim/KeyTable  
SigningTable            refile:/etc/opendkim/SigningTable  
SignatureAlgorithm      rsa-sha256  
Socket                  inet:8891@localhost  
PidFile                 /var/run/opendkim/opendkim.pid  
UMask                   022  
UserID                  opendkim:opendkim  
TemporaryDirectory      /var/tmp
EOF

sleep 5
echo "default._domainkey.[#DOMAIN#] [#DOMAIN#]:default:/etc/pmta/[#DOMAIN#]-dkim.key" >> /etc/opendkim/KeyTable
sleep 5
echo "*@[#DOMAIN#] default._domainkey.[#DOMAIN#]" >> /etc/opendkim/SigningTable
sleep 5
cat >> /etc/opendkim/TrustedHosts <<EOF
[#DOMAIN#]
titan0.[#DOMAIN#]
EOF


systemctl start opendkim
systemctl enable opendkim
systemctl restart postfix


### END

#service opendkim stop 
#chkconfig opendkim off 
systemctl start named
systemctl enable named

echo "
### <=========================================================================================> ###
Step 3 ### ---> Config mysqld - Mysql/System - CentOS 7 x86_64 = MYSQL ======================> ###
### <=========================================================================================> ###
"

mv /etc/my.cnf /etc/my.cnf-bkp

		echo "[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
user=mysql

symbolic-links=0
innodb_file_per_table = 1
thread_concurrency = 8
query_cache_size = 32M
thread_cache_size = 8
myisam_sort_buffer_size = 64M
read_rnd_buffer_size = 8M
read_buffer_size = 2M
sort_buffer_size = 2M
table_open_cache = 512
max_allowed_packet = 1M
key_buffer_size = 384M

[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
" > /etc/my.cnf

systemctl restart mysqld
systemctl enable mysqld


mysqladmin -uroot password [#SQLPASS#]
mysql -uroot -p[#SQLPASS#] -e "create database rainloop"

echo "
### <=========================================================================================> ###
Step 4 ### ---> PHP configuration and HTTPD - php-httpd-config.sh ============================> ###
### <=========================================================================================> ###
"
### date.timezone = America/Chicago
### date.timezone = Europe/Moscow
### ---> Configuration of PHP

mv /etc/php.ini /etc/php.ini-bkp

		echo "[PHP]

engine = On
zend.ze1_compatibility_mode = Off
short_open_tag = On
asp_tags = Off
precision = 14
y2k_compliance = On
output_buffering = 6000
zlib.output_compression = Off
implicit_flush = Off
unserialize_callback_func =
serialize_precision = 100
allow_call_time_pass_reference = Off
safe_mode = Off
safe_mode_gid = Off
safe_mode_include_dir =
safe_mode_exec_dir =
safe_mode_allowed_env_vars = PHP_
safe_mode_protected_env_vars = LD_LIBRARY_PATH
disable_functions =
disable_classes =
expose_php = On
max_execution_time = 2000    
max_input_time = 2000
memory_limit = 536870912
error_reporting = E_ALL
display_errors = Off
display_startup_errors = Off
log_errors = On
log_errors_max_len = 1200
ignore_repeated_errors = Off
ignore_repeated_source = Off
report_memleaks = On
track_errors = Off
variables_order = \"EGPCS\"
register_globals = Off
register_long_arrays = Off
register_argc_argv = Off
auto_globals_jit = On
post_max_size = 64M
magic_quotes_gpc = Off
magic_quotes_runtime = Off
magic_quotes_sybase = Off
auto_prepend_file =
auto_append_file =
default_mimetype = \"text/html\"
doc_root =
user_dir =
extension_dir = \"/usr/lib64/php/modules\"
enable_dl = On
file_uploads = On
upload_max_filesize = 64M
allow_url_fopen = On
default_socket_timeout = 120

[Date]
date.timezone = America/Chicago

[Syslog]
define_syslog_variables  = Off

[mail function]
SMTP = localhost
smtp_port = 25
sendmail_path = /usr/sbin/sendmail -t -i

[SQL]
sql.safe_mode = Off

[ODBC]
odbc.allow_persistent = On
odbc.check_persistent = On
odbc.max_persistent = -1
odbc.max_links = -1
odbc.defaultlrl = 6000
odbc.defaultbinmode = 1

[MySQL]
mysql.allow_persistent = On
mysql.max_persistent = -1
mysql.max_links = -1
mysql.default_port =
mysql.default_socket =
mysql.default_host =
mysql.default_user =
mysql.default_password =
mysql.connect_timeout = 120
mysql.trace_mode = Off

[MySQLi]
mysqli.max_links = -1
mysqli.default_port = 3306
mysqli.default_socket =
mysqli.default_host =
mysqli.default_user =
mysqli.default_pw =
mysqli.reconnect = Off

[mSQL]
msql.allow_persistent = On
msql.max_persistent = -1
msql.max_links = -1

[PostgresSQL]
pgsql.allow_persistent = On
pgsql.auto_reset_persistent = Off
pgsql.max_persistent = -1
pgsql.max_links = -1
pgsql.ignore_notice = 0
pgsql.log_notice = 0

[Sybase]
sybase.allow_persistent = On
sybase.max_persistent = -1
sybase.max_links = -1
sybase.min_error_severity = 10
sybase.min_message_severity = 10
sybase.compatability_mode = Off

[Sybase-CT]
sybct.allow_persistent = On
sybct.max_persistent = -1
sybct.max_links = -1
sybct.min_server_severity = 10
sybct.min_client_severity = 10

[bcmath]
bcmath.scale = 0

[Informix]
ifx.default_host =
ifx.default_user =
ifx.default_password =
ifx.allow_persistent = On
ifx.max_persistent = -1
ifx.max_links = -1
ifx.textasvarchar = 0
ifx.byteasvarchar = 0
ifx.charasvarchar = 0
ifx.blobinfile = 0
ifx.nullformat = 0

[Session]
session.save_handler = files
session.save_path = \"/var/lib/php/session\"
session.use_cookies = 1
session.name = PHPSESSID
session.auto_start = 0
session.cookie_lifetime = 0
session.cookie_path = /
session.cookie_domain =
session.serialize_handler = php
session.gc_probability = 1
session.gc_divisor     = 1000
session.gc_maxlifetime = 1440
session.bug_compat_42 = 0
session.bug_compat_warn = 1
session.referer_check =
session.entropy_length = 0
session.entropy_file =
session.cache_limiter = nocache
session.cache_expire = 180
session.use_trans_sid = 0
session.hash_function = 0
session.hash_bits_per_character = 5
url_rewriter.tags = \"a=href,area=href,frame=src,input=src,form=fakeentry\"

[MSSQL]
mssql.allow_persistent = On
mssql.max_persistent = -1
mssql.max_links = -1
mssql.min_error_severity = 10
mssql.min_message_severity = 10
mssql.compatability_mode = Off
mssql.secure_connection = Off

[Verisign Payflow Pro]
pfpro.defaulthost = \"test-payflow.verisign.com\"
pfpro.defaultport = 443
pfpro.defaulttimeout = 30

[Tidy]
tidy.clean_output = Off

[soap]
soap.wsdl_cache_enabled = 1
soap.wsdl_cache_dir = \"/tmp\"
soap.wsdl_cache_ttl = 86400

[Ioncube]
zend_extension = /usr/lib64/php/modules/ioncube_loader_lin_7.2.so

" > /etc/php.ini

### ---> Installation of Ioncube

mv /autosmtp/ioncube_loader_lin_7.2.so /usr/lib64/php/modules/ioncube_loader_lin_7.2.so
chmod 777 /usr/lib64/php/modules/ioncube_loader_lin_7.2.so

### ---> Apache configuration

mv /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf-bkp

echo '
ServerRoot "/etc/httpd"
Listen 80
Include conf.modules.d/*.conf
User apache
Group apache
ServerAdmin root@localhost
<Directory />
    AllowOverride none
    Require all denied
</Directory>
DocumentRoot "/var/www"
<Directory "/var/www">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>
<Directory "/var/www">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>
ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
   
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      # You need to enable mod_logio.c to use %I and %O
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>

    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>
    
    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types
    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz
    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>

EnableSendfile on

IncludeOptional conf.d/*.conf
' >> /etc/httpd/conf/httpd.conf

echo "<VirtualHost *:80>
    ServerAdmin webmaster@[#DOMAIN#]
    DocumentRoot /var/www
    ServerName [#DOMAIN#]
    ServerAlias *.[#DOMAIN#]
    ErrorLog logs/[#DOMAIN#].error_log
    CustomLog logs/[#DOMAIN#].acces_log common
</VirtualHost>" > /etc/httpd/conf.d/[#DOMAIN#].conf

echo "INSTALLING Default Page 
### <=========================================================================================> ###
"

rm -rf /var/www/html


echo '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to PmtaExpert.com Ultimate Email Marketing Solution</title>
<style type="text/css">
#apDiv1 {
	position: absolute;
	width: 200px;
	height: 115px;
	z-index: 1;
	left: 166px;
	top: 70px;
}
</style>
</head>

<body>
<div id="apDiv1"><img src="https://www.lyfemarketing.com/blog/wp-content/uploads/2018/03/email-marketing123.jpg" width="1024" height="544" /></div>
</body>
</html>
 ' > /var/www/index.html 
 
#### ----> Rainloop configuration 

mkdir -p /var/www/mail

unzip -q /autosmtp/rainloop.zip -d /var/www/mail
chown -R apache:apache /var/www/mail
find /var/www/mail -type d -exec chmod 755 {} \;
find /var/www/mail -type f -exec chmod 644 {} \;

echo "
imap_host = '[#DOMAIN#]'
imap_port = 143
imap_secure = 'TLS'
imap_short_login = On
sieve_use = Off
sieve_allow_raw = Off
sieve_host = ''
sieve_port = 4190
sieve_secure = 'None'
smtp_host = '[#DOMAIN#]'
smtp_port = 25
smtp_secure = 'None'
smtp_short_login = On
smtp_auth = Off
smtp_php_mail = Off
white_list = ''
"> /var/www/mail/data/_data_/_default_/domains/[#DOMAIN#].ini

echo '
; RainLoop Webmail configuration file
; Please dont add custom parameters here, those will be overwritten

[webmail]
; Text displayed as page title
title = "RainLoop Webmail"

; Text displayed on startup
loading_description = "RainLoop"
favicon_url = ""

; Theme used by default
theme = "Default"

; Allow theme selection on settings screen
allow_themes = On
allow_user_background = Off

; Language used by default
language = "en"

; Admin Panel interface language
language_admin = "en"

; Allow language selection on settings screen
allow_languages_on_settings = On
allow_additional_accounts = On
allow_additional_identities = On

;  Number of messages displayed on page by default
messages_per_page = 20

; File size limit (MB) for file upload on compose screen
; 0 for unlimited.
attachment_size_limit = 25

[interface]
show_attachment_thumbnail = On
use_native_scrollbars = Off
new_move_to_folder_button = On

[branding]
login_logo = ""
login_background = ""
login_desc = ""
login_css = ""
login_powered = On
user_css = ""
user_logo = ""
user_logo_title = ""
user_logo_message = ""
user_iframe_message = ""
welcome_page_url = ""
welcome_page_display = "none"

[contacts]
; Enable contacts
enable = On
allow_sync = Off
sync_interval = 20
type = "mysql"
pdo_dsn = "mysql:host=127.0.0.1;port=3306;dbname=rainloop"
pdo_user = "root"
pdo_password = "[#SQLPASS#]"
suggestions_limit = 30

[security]
; Enable CSRF protection (http://en.wikipedia.org/wiki/Cross-site_request_forgery)
csrf_protection = On
custom_server_signature = "RainLoop"
x_frame_options_header = ""
openpgp = Off

; Login and password for web admin panel
admin_login = "admin"
admin_password = "12345"

; Access settings
allow_admin_panel = On
allow_two_factor_auth = Off
force_two_factor_auth = Off
hide_x_mailer_header = Off
admin_panel_host = ""
admin_panel_key = "admin"
content_security_policy = ""
core_install_access_domain = ""

[ssl]
; Require verification of SSL certificate used.
verify_certificate = Off

; Allow self-signed certificates. Requires verify_certificate.
allow_self_signed = On

; Location of Certificate Authority file on local filesystem (/etc/ssl/certs/ca-certificates.crt)
cafile = ""

; capath must be a correctly hashed certificate directory. (/etc/ssl/certs/)
capath = ""

[capa]
folders = On
composer = On
contacts = On
settings = On
quota = On
help = On
reload = On
search = On
search_adv = On
filters = On
x-templates = Off
dangerous_actions = On
message_actions = On
messagelist_actions = On
attachments_actions = On

[login]
default_domain = "[#DOMAIN#]"

; Allow language selection on webmail login screen
allow_languages_on_login = On
determine_user_language = On
determine_user_domain = Off
welcome_page = Off
hide_submit_button = On
forgot_password_link_url = ""
registration_link_url = ""
login_lowercase = On

; This option allows webmail to remember the logged in user
; once they closed the browser window.
; 
; Values:
;   "DefaultOff" - can be used, disabled by default;
;   "DefaultOn"  - can be used, enabled by default;
;   "Unused"     - cannot be used
sign_me_auto = "DefaultOff"

[plugins]
; Enable plugin support
enable = On

; List of enabled plugins
enabled_list = ""

[defaults]
; Editor mode used by default (Plain, Html, HtmlForced or PlainForced)
view_editor_type = "Html"

; layout: 0 - no preview, 1 - side preview, 2 - bottom preview
view_layout = 1
view_use_checkboxes = On
autologout = 30
show_images = Off
contacts_autosave = On
mail_use_threads = Off
allow_draft_autosave = On
mail_reply_same_folder = Off

[logs]
; Enable logging
enable = Off

; Logs entire request only if error occured (php requred)
write_on_error_only = Off

; Logs entire request only if php error occured
write_on_php_error_only = Off

; Logs entire request only if request timeout (in seconds) occured.
write_on_timeout_only = 0

; Required for development purposes only.
; Disabling this option is not recommended.
hide_passwords = On
time_offset = "0"
session_filter = ""

; Log filename.
; For security reasons, some characters are removed from filename.
; Allows for pattern-based folder creation (see examples below).
; 
; Patterns:
;   {date:Y-m-d}  - Replaced by pattern-based date
;                   Detailed info: http://www.php.net/manual/en/function.date.php
;   {user:email}  - Replaced by users email address
;                   If user is not logged in, value is set to "unknown"
;   {user:login}  - Replaced by users login (the user part of an email)
;                   If user is not logged in, value is set to "unknown"
;   {user:domain} - Replaced by users domain name (the domain part of an email)
;                   If user is not logged in, value is set to "unknown"
;   {user:uid}    - Replaced by users UID regardless of account currently used
; 
;   {user:ip}
;   {request:ip}  - Replaced by users IP address
; 
; Others:
;   {imap:login} {imap:host} {imap:port}
;   {smtp:login} {smtp:host} {smtp:port}
; 
; Examples:
;   filename = "log-{date:Y-m-d}.txt"
;   filename = "{date:Y-m-d}/{user:domain}/{user:email}_{user:uid}.log"
;   filename = "{user:email}-{date:Y-m-d}.txt"
filename = "log-{date:Y-m-d}.txt"

; Enable auth logging in a separate file (for fail2ban)
auth_logging = Off
auth_logging_filename = "fail2ban/auth-{date:Y-m-d}.txt"
auth_logging_format = "[{date:Y-m-d H:i:s}] Auth failed: ip={request:ip} user={imap:login} host={imap:host} port={imap:port}"

[debug]
; Special option required for development purposes
enable = Off

[social]
; Google
google_enable = Off
google_enable_auth = Off
google_enable_auth_fast = Off
google_enable_drive = Off
google_enable_preview = Off
google_client_id = ""
google_client_secret = ""
google_api_key = ""

; Facebook
fb_enable = Off
fb_app_id = ""
fb_app_secret = ""

; Twitter
twitter_enable = Off
twitter_consumer_key = ""
twitter_consumer_secret = ""

; Dropbox
dropbox_enable = Off
dropbox_api_key = ""

[cache]
; The section controls caching of the entire application.
; 
; Enables caching in the system
enable = On

; Additional caching key. If changed, cache is purged
index = "v1"

; Can be: files, APC, memcache, redis (beta)
fast_cache_driver = "files"

; Additional caching key. If changed, fast cache is purged
fast_cache_index = "v1"

; Browser-level cache. If enabled, caching is maintainted without using files
http = On

; Browser-level cache time (seconds, Expires header)
http_expires = 3600

; Caching message UIDs when searching and sorting (threading)
server_uids = On

[labs]
; Experimental settings. Handle with care.
; 
allow_mobile_version = On
ignore_folders_subscription = Off
check_new_password_strength = On
update_channel = "stable"
allow_gravatar = On
allow_prefetch = On
allow_smart_html_links = On
cache_system_data = On
date_from_headers = On
autocreate_system_folders = On
allow_message_append = Off
disable_iconv_if_mbstring_supported = Off
login_fault_delay = 1
log_ajax_response_write_limit = 300
allow_html_editor_source_button = Off
allow_html_editor_biti_buttons = Off
allow_ctrl_enter_on_compose = On
try_to_detect_hidden_images = Off
hide_dangerous_actions = Off
use_app_debug_js = Off
use_mobile_version_for_tablets = Off
use_app_debug_css = Off
use_imap_sort = On
use_imap_force_selection = Off
use_imap_list_subscribe = On
use_imap_thread = On
use_imap_move = Off
use_imap_expunge_all_on_delete = Off
imap_forwarded_flag = "$Forwarded"
imap_read_receipt_flag = "$ReadReceipt"
imap_body_text_limit = 555000
imap_message_list_fast_simple_search = On
imap_message_list_count_limit_trigger = 0
imap_message_list_date_filter = 0
imap_message_list_permanent_filter = ""
imap_message_all_headers = Off
imap_large_thread_limit = 50
imap_folder_list_limit = 200
imap_show_login_alert = On
imap_use_auth_plain = On
imap_use_auth_cram_md5 = Off
smtp_show_server_errors = Off
smtp_use_auth_plain = On
smtp_use_auth_cram_md5 = Off
sieve_allow_raw_script = Off
sieve_utf8_folder_name = On
sieve_auth_plain_initial = On
sieve_allow_fileinto_inbox = Off
imap_timeout = 300
smtp_timeout = 60
sieve_timeout = 10
domain_list_limit = 99
mail_func_clear_headers = On
mail_func_additional_parameters = Off
favicon_status = On
folders_spec_limit = 50
owncloud_save_folder = "Attachments"
owncloud_suggestions = On
curl_proxy = ""
curl_proxy_auth = ""
in_iframe = Off
force_https = Off
custom_login_link = ""
custom_logout_link = ""
allow_external_login = Off
allow_external_sso = Off
external_sso_key = ""
http_client_ip_check_proxy = Off
fast_cache_memcache_host = "127.0.0.1"
fast_cache_memcache_port = 11211
fast_cache_redis_host = "127.0.0.1"
fast_cache_redis_port = 6379
use_local_proxy_for_external_images = Off
detect_image_exif_orientation = On
cookie_default_path = ""
cookie_default_secure = Off
check_new_messages = On
replace_env_in_configuration = ""
startup_url = ""
strict_html_parser = Off
allow_cmd = Off
dev_email = ""
dev_password = ""

[version]
current = "1.12.0"
saved = "Thu, 24 Jun 2021 23:22:16 +0000"
'> /var/www/mail/data/_data_/_default_/configs/application.ini

mv /etc/httpd/conf.d/phpMyAdmin.conf /etc/httpd/conf.d/phpMyAdmin.conf-bkp

echo '
### ~> PmtaExpert.com PhpMyAdmin 
Alias /phpMyAdmin /usr/share/phpMyAdmin
Alias /phpmyadmin /usr/share/phpMyAdmin

<Directory /usr/share/phpMyAdmin/>
   AddDefaultCharset UTF-8

   <IfModule mod_authz_core.c>
     # Apache 2.4
     <RequireAny>
       #Require ip 127.0.0.1
       #Require ip ::1
       Require all granted
     </RequireAny>
   </IfModule>
   <IfModule !mod_authz_core.c>
     # Apache 2.2
     Order Deny,Allow
     Deny from All
     Allow from 127.0.0.1
     Allow from ::1
   </IfModule>
</Directory>

<Directory /usr/share/phpMyAdmin/setup/>
   <IfModule mod_authz_core.c>
     # Apache 2.4
     <RequireAny>
	 Require ip ::1
     </RequireAny>
   </IfModule>
   <IfModule !mod_authz_core.c>
     # Apache 2.2
     Order Deny,Allow
     Deny from All
     Allow from ::1
   </IfModule>
</Directory>


<Directory /usr/share/phpMyAdmin/libraries/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

<Directory /usr/share/phpMyAdmin/setup/lib/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

<Directory /usr/share/phpMyAdmin/setup/frames/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>
' > /etc/httpd/conf.d/phpMyAdmin.conf

chown apache:apache /var/www/ -R 
systemctl restart httpd
systemctl enable httpd

echo "
### <=========================================================================================> ###
Step 5 ### ---> Configure Dovecot - Postfix ==================================================> ###
### <=========================================================================================> ###
"

echo '### ~> Dovecot conf 
protocols = imap pop3 lmtp
listen = *
dict {
}
!include conf.d/*.conf
' > /etc/dovecot/dovecot.conf

echo '### ~> Dovecot mail.conf 
mail_location = maildir:~/Maildir
mbox_write_locks = fcntl
' > /etc/dovecot/conf.d/10-mail.conf

echo '### ~> Dovecot pop3.conf 
protocol pop3 {
  pop3_uidl_format = %08Xu%08Xv
  pop3_client_workarounds = outlook-no-nuls oe-ns-eoh
}
' > /etc/dovecot/conf.d/20-pop3.conf

echo '### ~> Dovecot master.conf 
service imap-login {
  inet_listener imap {
  }
  inet_listener imaps {
  }
}

service pop3-login {
  inet_listener pop3 {
  }
  inet_listener pop3s {
  }
}

service lmtp {
  unix_listener lmtp {
  }
}

service imap {
}

service pop3 {
}

service auth {
  unix_listener auth-userdb {
  }
  unix_listener /var/spool/postfix/private/auth {
    mode = 0666
	user = postfix
	group = postfix
  }
}

service auth-worker {
}

service dict {
  unix_listener dict {
  }
}
' >  /etc/dovecot/conf.d/10-master.conf

echo '### ~> Dovecot mail.conf 
mail_location = maildir:~/Maildir

namespace inbox {
  
  inbox = yes

}

first_valid_uid = 1000

' >  /etc/dovecot/conf.d/10-mail.conf

echo '### ~> Dovecot auth.conf 
auth_mechanisms = plain login
!include auth-system.conf.ext
' > /etc/dovecot/conf.d/10-auth.conf

mv /etc/postfix/main.cf /etc/postfix/main.cf-bkp

echo '### ~> Postfix main.cf 
queue_directory = /var/spool/postfix
command_directory = /usr/sbin
daemon_directory = /usr/libexec/postfix
mail_owner = postfix
' > /etc/postfix/main.cf
echo "myhostname = [#DOMAIN#]
mydomain = [#DOMAIN#]" >> /etc/postfix/main.cf
echo 'myorigin = $mydomain
inet_interfaces = 127.0.0.1
inet_protocols = ipv4
mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain
unknown_local_recipient_reject_code = 550' >> /etc/postfix/main.cf
echo "mynetworks = 127.0.0.1, $IpPrinc" >> /etc/postfix/main.cf
echo 'alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
home_mailbox = Maildir/
mail_spool_directory = /var/spool/mail
smtpd_banner = $myhostname ESMTP $mail_name
debug_peer_level = 2
debugger_command =
	 PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
	 xxgdb $daemon_directory/$process_name $process_id & sleep 5
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix
setgid_group = postdrop
html_directory = no
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix-2.10.1/samples
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
smtpd_sasl_auth_enable = yes
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth
smtpd_sasl_authenticated_header = yes
smtpd_recipient_restrictions =  permit_mynetworks, 
    permit_sasl_authenticated, 
    reject_unauth_destination
broken_sasl_auth_clients = yes
delay_warning_time = 2h
fast_flush_refresh_time = 15m
fast_flush_purge_time = 30m
smtpd_recipient_limit = 776
bounce_queue_lifetime = 30m
maximal_queue_lifetime = 2h
mailbox_size_limit = 0
message_size_limit = 0
smtp_connection_cache_on_demand = no
smtpd_peername_lookup = no
default_process_limit = 776
qmgr_message_active_limit = 40000
qmgr_message_recipient_limit = 40000
default_destination_concurrency_limit = 776
default_destination_recipient_limit = 776
smtp_mx_session_limit = 776
smtpd_client_connection_count_limit = 400
smtp_destination_concurrency_limit = 400
maximal_backoff_time = 1000s
minimal_backoff_time = 300s
smtpd_milters = inet:127.0.0.1:8891
non_smtpd_milters = $smtpd_milters
milter_default_action = accept' >> /etc/postfix/main.cf

mv /etc/postfix/master.cf /etc/postfix/master.cf-bkp

echo '### ~> Postfix master.cf 
2525      inet  n       -       n       -       100      smtpd
pickup    fifo  n       -       n       60      1       pickup
cleanup   unix  n       -       n       -       0       cleanup
qmgr      fifo  n       -       n       300     1       qmgr
tlsmgr    unix  -       -       n       500?    1       tlsmgr
rewrite   unix  -       -       n       -       -       trivial-rewrite
bounce    unix  -       -       n       -       0       bounce
defer     unix  -       -       n       -       0       bounce
trace     unix  -       -       n       -       0       bounce
verify    unix  -       -       n       -       1       verify
flush     unix  n       -       n       500?    0       flush
proxymap  unix  -       -       n       -       -       proxymap
smtp      unix  -       -       n       -       100       smtp

relay     unix  -       -       n       -       -       smtp
	-o fallback_relay=

showq     unix  n       -       n       -       -       showq
error     unix  -       -       n       -       -       error
discard   unix  -       -       n       -       -       discard
local     unix  -       n       n       -       -       local
virtual   unix  -       n       n       -       -       virtual
lmtp      unix  -       -       n       -       -       lmtp
anvil     unix  -       -       n       -       1       anvil
scache	  unix	-	-	n	-	1	scache

maildrop  unix  -       n       n       -       -       pipe
	flags=DRhu user=vmail argv=/usr/local/bin/maildrop -d ${recipient}

old-cyrus unix  -       n       n       -       -       pipe
	flags=R user=cyrus argv=/usr/lib/cyrus-imapd/deliver -e -m ${extension} ${user}

cyrus     unix  -       n       n       -       -       pipe
	user=cyrus argv=/usr/lib/cyrus-imapd/deliver -e -r ${sender} -m ${extension} ${user}

uucp      unix  -       n       n       -       -       pipe
	flags=Fqhu user=uucp argv=uux -r -n -z -a$sender - $nexthop!rmail ($recipient)

ifmail    unix  -       n       n       -       -       pipe
	flags=F user=ftn argv=/usr/lib/ifmail/ifmail -r $nexthop ($recipient)

bsmtp     unix  -       n       n       -       -       pipe
	flags=Fq. user=foo argv=/usr/local/sbin/bsmtp -f $sender $nexthop $recipient' > /etc/postfix/master.cf

echo "Configure sysctl.conf : 
### <=========================================================================================> ###
"

mv /etc/sysctl.conf /etc/sysctl.conf-bkp

echo '# Kernel sysctl configuration file for Red Hat Linux

net.ipv4.ip_forward = 0
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.default.accept_source_route = 0
kernel.sysrq = 0
kernel.core_uses_pid = 1
net.ipv4.tcp_syncookies = 1
kernel.msgmnb = 65536
kernel.msgmax = 65536
kernel.shmmax = 68719476736
kernel.shmall = 4294967296

#ALTERAÇÕES PARA POSTFIX
fs.file-max = 16384
kernel.threads-max = 2048 ' > /etc/sysctl.conf

systemctl stop sendmail
systemctl disable sendmail
systemctl restart dovecot
systemctl enable dovecot
systemctl restart postfix
/usr/sbin/postalias /etc/aliases
systemctl enable postfix
systemctl restart saslauthd
systemctl enable saslauthd
echo "Configuring OpenSSL : 
### <=========================================================================================> ###
"
mkdir /etc/ssl/private 
openssl req -new -x509 -days 3650 -nodes -out "/etc/ssl/private/pmta.[#DOMAIN#].cert" -keyout "/etc/ssl/private/pmta.[#DOMAIN#].key" -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=[#DOMAIN#]"

cat /etc/ssl/private/pmta.[#DOMAIN#].cert /etc/ssl/private/pmta.[#DOMAIN#].key > /etc/ssl/private/pmta.[#DOMAIN#].pem

echo "
### <=========================================================================================> ###
Step 6 ### ---> Installing and configuring PowerMTA- pmta-config.sh ==========================> ###
### <=========================================================================================> ###
"

echo '* soft nofile 16384' >> /etc/security/limits.conf
echo '* hard nofile 32768' >> /etc/security/limits.conf

sleep 3
rpm -ivh /autosmtp/PowerMTA-5.0r7.rpm
sleep 3
yes | cp -fr /autosmtp/patch/* /

ArqPmtaConfig=/etc/pmta/config
ArqPmtaConfig2=/tmp/arqpmtaconfig2.info
ArqPmtaConfig3=/tmp/arqpmtaconfig3.info

if [ $IpsQuant -lt 8 ]
then
	ConexPmta=`expr $IpsQuant \* 5`
	MaxEnvios=`expr $IpsQuant \* 3200`
	QuantConfigPmta=0
	cat /root/autosmtp-scripts/ips.info > /tmp/ipspmtacfg.info
else
	ConexPmta=`expr \( $IpsQuant - 1 \) \* 5`
	MaxEnvios=`expr \( $IpsQuant - 1 \) \* 3200`
	QuantConfigPmta=1
	sed '1d' /root/autosmtp-scripts/ips.info > /tmp/ipspmtacfg.info
fi

IpsPmtaConfig=`cat /tmp/ipspmtacfg.info`

echo 'host-name [#DOMAIN#]
relay-domain [#DOMAIN#]
<domain [#DOMAIN#]>
         smtp-hosts [127.0.0.1]:25
         
</domain>



total-max-smtp-out 400 #amount of threads you want mailer to send at
total-max-smtp-in 1000 #amount of threads you want to allow for incoming mails

############################################################################
# BEGIN: ISP rules
############################################################################

domain-macro tld com, de, co.uk, es, net, ca, fr, com.tw, in, org, de, com.cn, com.tr, com.ph, com.br, com.au, com.vn, com.sg, co.in, co.id, co.nz, com.mx, ie, co.kr
domain-macro aolyahoo yahoo, ymail, y7mail, rocketmail, sbcglobal, att, bellsouth, aol, aim, wmconnect.com, netscape.net, compuserve, cs, marktoob, oddpost, kimo, geocities, schms, btinternet, rogers, rocketmail, verizon, flash, icloud
domain-macro hotmail hotmail, live, msn, outlook
domain-macro gmail gmail
domain-macro other cox, comcast, charter, rr

<domain *>
smtp-pattern-list backoff
smtp-pattern-list blocking-errors
smtp-greeting-timeout 5m
dkim-sign yes
dkim-algorithm rsa-sha1
dkim-body-canon relaxed
dkim-headers-canon relaxed
dkim-add-body-limit no
dkim-add-timestamp no
dkim-expire-after never
max-msg-rate 500/h
use-starttls yes
require-starttls no
track-recipient-events true
bounce-after 1m
bounce-upon-no-mx yes
</domain>

<domain $aolyahoo.$tld>
  smtp-pattern-list backoff
  smtp-pattern-list blocking-errors
  backoff-to-normal-after 1h
  queue-priority  75
  smtp-421-means-mx-unavailable yes
  replace-smtp-421-service-response yes
  max-msg-rate 500/h ## ay ##
  max-connect-rate 10/h ## ay ##
  retry-after 20m,20m,20m,30m,30m,1h,2h,2h,4h,4h
  backoff-max-msg-rate 2/h
  backoff-upon-all-sources-disabled yes
  backoff-to-normal-after-delivery yes
  bounce-upon-5xx-greeting false
</domain>

<domain $hotmail.$tld>
  smtp-pattern-list backoff
  smtp-pattern-list blocking-errors
  max-msg-rate 500/h ## hotmail ##
  max-errors-per-connection 10
  max-connect-rate 80/h ## hotmail ##
  backoff-to-normal-after 4h
  bounce-upon-5xx-greeting yes
  smtp-421-means-mx-unavailable yes
</domain>

<domain $gmail.$tld>
  smtp-pattern-list backoff
  smtp-pattern-list blocking-errors
  max-msg-rate 500/h ## gmail ##
  max-connect-rate 10/h ## gmail ##
  backoff-to-normal-after 4h
  smtp-421-means-mx-unavailable yes
</domain>

<domain $other.$tld>
  smtp-pattern-list backoff
  smtp-pattern-list blocking-errors
  max-msg-rate 3200/h
  max-connect-rate 10/h
  backoff-to-normal-after 4h
  queue-priority  75
  bounce-upon-5xx-greeting yes
  smtp-421-means-mx-unavailable yes
  backoff-to-normal-after-delivery yes
  connect-timeout 1m
  smtp-greeting-timeout 5m
  dkim-sign yes
  dkim-algorithm rsa-sha1
  dkim-body-canon relaxed
  dkim-headers-canon relaxed
  dkim-add-body-limit no
  dkim-add-timestamp no
  dkim-expire-after never
  use-starttls TRUE
  require-starttls no
</domain>
' > $ArqPmtaConfig

echo '
mx-connection-limit * 999999

#Domains to Block Based on MX
mx-connection-limit [*.]xmission.com 0
mx-connection-limit [*.]xmission.net 0
mx-connection-limit [*.]isux.com 0
mx-connection-limit [*.]isux.net 0
mx-connection-limit [*.]isux.org 0
mx-connection-limit [*.]sorbs.net 0
mx-connection-limit [*.]sorbs.com 0
mx-connection-limit [*.]usfamily.net 0
mx-connection-limit [*.]usfamily.com 0
mx-connection-limit [*.]spamexperts.com 0

' >> $ArqPmtaConfig

echo '
######

############################################################################
# END: ISP rules
############################################################################

############################################################################
# BEGIN: BACKOFF RULES
############################################################################

<smtp-pattern-list backoff>
reply /generating high volumes of.* complaints from AOL/ mode=backoff
reply /Excessive unknown recipients - possible Open Relay/ mode=backoff
reply /^421 .* too many errors/ mode=backoff
reply /blocked.*spamhaus/ mode=backoff
reply /451 Rejected/ mode=backoff
</smtp-pattern-list>

<smtp-pattern-list blocking-errors>
reply /421 .* SERVICE NOT AVAILABLE/ mode=backoff
reply /generating high volumes of.* complaints from AOL/ mode=backoff
reply /554 .*aol.com/ mode=backoff
reply /421dynt1/ mode=backoff
reply /HVU:B1/ mode=backoff
reply /DNS:NR/ mode=backoff
reply /RLY:NW/ mode=backoff
reply /DYN:T1/ mode=backoff
reply /RLY:BD/ mode=backoff
reply /RLY:CH2/ mode=backoff
reply /421 .* Please try again later/ mode=backoff
reply /421 Message temporarily deferred/ mode=backoff
reply /VS3-IP5 Excessive unknown recipients/ mode=backoff
reply /VSS-IP Excessive unknown recipients/ mode=backoff
reply /\[GL01\] Message from/ mode=backoff
reply /\[TS01\] Messages from/ mode=backoff
reply /\[TS02\] Messages from/ mode=backoff
reply /\[TS03\] All messages from/ mode=backoff
reply /exceeded the rate limit/ mode=backoff
reply /exceeded the connection limit/ mode=backoff
reply /Mail rejected by Windows Live Hotmail for policy reasons/ mode=backoff
reply /mail.live.com\/mail\/troubleshooting.aspx/ mode=backoff
reply /421 Message Rejected/ mode=backoff
reply /Client host rejected/ mode=backoff
reply /blocked using UCEProtect/ mode=backoff
reply /Mail Refused/ mode=backoff
reply /421 Exceeded allowable connection time/ mode=backoff
reply /amIBlockedByRR/ mode=backoff
reply /block-lookup/ mode=backoff
reply /Too many concurrent connections from source IP/ mode=backoff
reply /too many/ mode=backoff
reply /Exceeded allowable connection time/ mode=backoff
reply /Connection rate limit exceeded/ mode=backoff
reply /refused your connection/ mode=backoff
reply /try again later/ mode=backoff
reply /try later/ mode=backoff
reply /550 RBL/ mode=backoff
reply /TDC internal RBL/ mode=backoff
reply /connection refused/ mode=backoff
reply /please see www.spamhaus.org/ mode=backoff
reply /Message Rejected/ mode=backoff
reply /refused by antispam/ mode=backoff
reply /Service not available/ mode=backoff
reply /currently blocked/ mode=backoff
reply /locally blacklisted/ mode=backoff
reply /not currently accepting mail from your ip/ mode=backoff
reply /421.*closing connection/ mode=backoff
reply /421.*Lost connection/ mode=backoff
reply /476 connections from your host are denied/ mode=backoff
reply /421 Connection cannot be established/ mode=backoff
reply /421 temporary envelope failure/ mode=backoff
reply /421 4.4.2 Timeout while waiting for command/ mode=backoff
reply /450 Requested action aborted/ mode=backoff
reply /550 Access denied/ mode=backoff
reply /exceeded the rate limit/ mode=backoff
reply /421rlynw/ mode=backoff
reply /permanently deferred/ mode=backoff
reply /\d+\.\d+\.\d+\.\d+ blocked/ mode=backoff
reply /www\.spamcop\.net\/bl\.shtml/ mode=backoff
reply /generating high volumes of.* complaints from AOL/ mode=backoff
reply /Excessive unknown recipients - possible Open Relay/ mode=backoff
reply /^421 .* too many errors/ mode=backoff
reply /blocked.*spamhaus/ mode=backoff
reply /451 Rejected/ mode=backoff
</smtp-pattern-list>

############################################################################
# END: BACKOFF RULES
############################################################################

############################################################################
# BEGIN: BOUNCE RULES
############################################################################

<bounce-category-patterns>
/spam/ spam-related
/junk mail/ spam-related
/blacklist/ spam-related
/blocked/ spam-related
/\bU\.?C\.?E\.?\b/ spam-related
/\bAdv(ertisements?)?\b/ spam-related
/unsolicited/ spam-related
/\b(open)?RBL\b/ spam-related
/realtime blackhole/ spam-related
/http:\/\/basic.wirehub.nl\/blackholes.html/ spam-related
/\bvirus\b/ virus-related
/message +content/ content-related
/content +rejected/ content-related
/quota/ quota-issues
/limit exceeded/ quota-issues
/mailbox +(is +)?full/ quota-issues
/\bstorage\b/ quota-issues
/(user|mailbox|recipient|rcpt|local part|address|account|mail drop|ad(d?)ressee) (has|has been|is)? *(currently|temporarily 
+)?(disabled|expired|inactive|not

activated)/ inactive-mailbox
/(conta|usu.rio) inativ(a|o)/ inactive-mailbox
/Too many (bad|invalid|unknown|illegal|unavailable) (user|mailbox|recipient|rcpt|local part|address|account|mail drop|ad(d?)ressee)/ other
/(No such|bad|invalid|unknown|illegal|unavailable) (local +)?(user|mailbox|recipient|rcpt|local part|address|account|mail drop|ad(d?)ressee)/ bad-mailbox
/(user|mailbox|recipient|rcpt|local part|address|account|mail drop|ad(d?)ressee) +(\S+@\S+ +)?(not (a +)?valid|not known|not here|not found|does not 
exist|bad|

invalid|unknown|illegal|unavailable)/ bad-mailbox
/\S+@\S+ +(is +)?(not (a +)?valid|not known|not here|not found|does not exist|bad|invalid|unknown|illegal|unavailable)/ bad-mailbox
/no mailbox here by that name/ bad-mailbox
/my badrcptto list/ bad-mailbox
/not our customer/ bad-mailbox
/no longer (valid|available)/ bad-mailbox
/have a \S+ account/ bad-mailbox
/\brelay(ing)?/ relaying-issues
/domain (retired|bad|invalid|unknown|illegal|unavailable)/ bad-domain
/domain no longer in use/ bad-domain
/domain (\S+ +)?(is +)?obsolete/ bad-domain
/denied/ policy-related
/prohibit/ policy-related
/rejected/ policy-related
/refused/ policy-related
/allowed/ policy-related
/banned/ policy-related
/policy/ policy-related
/suspicious activity/ policy-related
/bad sequence/ protocol-errors
/syntax error/ protocol-errors
/\broute\b/ routing-errors
/\bunroutable\b/ routing-errors
/\bunrouteable\b/ routing-errors
/^2.\d.\d/ success
/^[45]\.1\.1/ bad-mailbox
/^[45]\.1\.2/ bad-domain
/^[45]\.3\.5/ bad-configuration
/^[45]\.4\.1/ no-answer-from-host
/^[45]\.4\.2/ bad-connection
/^[45]\.4\.4/ routing-errors
/^[45]\.4\.6/ routing-errors
/^[45]\.4\.7/ message-expired
/^[45]\.7\.1/ policy-related
// other    # catch-all
/failed\,5\.0\.0 \(undefined status\)\,x\-pmta\;bounce\-queue/ bad-domain
</bounce-category-patterns>

############################################################################
# END: BOUNCE RULES
############################################################################

############################################################################
# BEGIN: OTHER OPTIONS
############################################################################
' >> $ArqPmtaConfig

echo "postmaster postmaster@[#DOMAIN#]
smtp-listener 127.0.0.1:25 tls=yes 

<source 0/0> 
always-allow-relaying no
</source> 

<source 1.2.3.4> 
allow-unencrypted-plain-auth yes
always-allow-relaying yes
process-x-virtual-mta yes
allow-starttls yes
jobid-header x-job
process-x-job yes
hide-message-source true 
suppress-local-dsn no
max-message-size 25M
</source> 


sync-msg-create false 
sync-msg-update false 
run-as-root no 
log-file /var/log/pmta/log # logrotate is used for rotation 

<acct-file /var/log/pmta/acct.csv>
#    move-to /opt/myapp/pmta-acct   # configure as fit for your application
#    move-interval 5m
max-size 50M
</acct-file>

# transient errors (soft bounces)
<acct-file /var/log/pmta/diag.csv>
move-interval 1d
delete-after never
records t
</acct-file>

<domain {gmImprinter}>
max-events-recorded 150
log-messages yes
log-data no # extremely verbose, for debugging only
retry-after 15s
</domain>

#
# spool directories
#
<spool /var/spool/pmta> 
delete-file-holders yes
</spool> 

http-mgmt-port 1212 
http-access 0/0 monitor

http-redirect-to-https false

#SSL
smtp-server-tls-certificate /etc/ssl/private/pmta.[#DOMAIN#].pem

#######################################
# END: OTHER OPTIONS
############################################################################


############################################################################
# BEGIN: USERS/VIRTUAL-MTA / VIRTUAL-MTA-POOL / VIRTUAL-PMTA-PATTERN
############################################################################

<smtp-user pmtadm> 
password pmtadm@123
source {pmta-auth} 
</smtp-user>

<source {pmta-auth}>
       smtp-service yes
    always-allow-relaying yes
    require-auth true
    process-x-virtual-mta yes
    default-virtual-mta pmta-pool
    remove-received-headers true
    add-received-header false
    hide-message-source true
    remove-header X-PreviewEmail,X-Mailer,X-Mailer-RecptId,X-Mailer-Sent-By,X-Priority
	allow-starttls yes
</source>

 
" >> $ArqPmtaConfig

echo "<virtual-mta-pool pmta-pool>" > $ArqPmtaConfig2
echo "<pattern-list pmta-pattern>" > $ArqPmtaConfig3

for Ip in $IpsPmtaConfig
do
	echo " " >> $ArqPmtaConfig
	echo "<virtual-mta pmta-vmta$QuantConfigPmta>" >> $ArqPmtaConfig
	echo "virtual-mta pmta-vmta$QuantConfigPmta" >> $ArqPmtaConfig2
	echo "mail-from /@titan$QuantConfigPmta.[#DOMAIN#]/ virtual-mta=pmta-vmta$QuantConfigPmta" >> $ArqPmtaConfig3
	echo "smtp-source-host $Ip titan$QuantConfigPmta.[#DOMAIN#]" >> $ArqPmtaConfig
	echo "domain-key default,[#DOMAIN#],/etc/pmta/[#DOMAIN#]-dkim.key" >> $ArqPmtaConfig
	echo "max-smtp-msg-rate 3200/h" >> $ArqPmtaConfig
	echo "</virtual-mta>" >> $ArqPmtaConfig
	echo "<domain titan$QuantConfigPmta.[#DOMAIN#]>" >> $ArqPmtaConfig
	echo "</domain>" >> $ArqPmtaConfig
	echo " " >> $ArqPmtaConfig
	QuantConfigPmta=`expr $QuantConfigPmta + 1`
done

echo "</virtual-mta-pool>
" >> $ArqPmtaConfig2

echo "</pattern-list>
" >> $ArqPmtaConfig3

cat $ArqPmtaConfig2 >> $ArqPmtaConfig && cat $ArqPmtaConfig3 >> $ArqPmtaConfig

mv /tmp/dkim-default /etc/pmta/[#DOMAIN#]-dkim.key
chown pmta:pmta /etc/pmta/ -R
chown pmta:pmta /usr/sbin/pmtahttpd
chmod 755 /usr/sbin/pmtahttpd

systemctl start pmta
systemctl start pmtahttp
systemctl enable pmta 
systemctl enable pmtahttp
#systemctl restart pmtahttp

echo "
### <=========================================================================================> ###
Step 7 ### ---> other settings ==============================================================> ###
### <=========================================================================================> ###
"

### ---> Squid configuration

mv /etc/squid/squid.conf /etc/squid/squid.conf-bkp 
cat /etc/squid/squid.conf-bkp | egrep -vi '^#|^$' > /etc/squid/squid.conf 
sed -i 's/http_access deny all/http_access allow all/' /etc/squid/squid.conf
sed -i 's/http_port 3128/http_port 54321/' /etc/squid/squid.conf
echo 'visible_hostname localhost' >> /etc/squid/squid.conf
echo '/var/log/squid/store.log /var/log/squid/cache.log 
/var/log/squid/access.log { 
     weekly 
     rotate 5 
     copytruncate 
     compress 
     missingok 
     sharedscripts 
     postrotate 
       /usr/sbin/squid -k rotate 
     endscript 
} ' > /etc/logrotate.d/squid

systemctl disable squid

### ---> SSH configuration

mv /etc/ssh/sshd_config /etc/ssh/sshd_config-bkp

echo "### ---> SSH ESPECIAL 
Port 22
Protocol 2
SyslogFacility AUTHPRIV
PasswordAuthentication yes
ChallengeResponseAuthentication no
GSSAPIAuthentication yes
GSSAPICleanupCredentials yes
UsePAM yes
AcceptEnv LANG LANGUAGE LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES 
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT 
AcceptEnv LC_IDENTIFICATION LC_ALL
Subsystem	sftp	/usr/libexec/openssh/sftp-server
" > /etc/ssh/sshd_config

mv /etc/aliases /etc/aliases-bkp

echo "#
#  Aliases in this file will NOT be expanded in the header from
#  Mail, but WILL be visible over networks or from /bin/mail.
#
#	>>>>>>>>>>	The program newaliases must be run after
#	>> NOTE >>	this file is updated for any changes to
#	>>>>>>>>>>	show through to sendmail.
#

# Basic system aliases -- these MUST be present.
mailer-daemon:	postmaster
postmaster:	postmaster

# General redirections for pseudo accounts.
bin:		root
daemon:		root
adm:		root
lp:		root
sync:		root
shutdown:	root
halt:		root
mail:		root
news:		news
uucp:		root
operator:	root
games:		root
gopher:		root
ftp:		root
nobody:		root
radiusd:	root
nut:		root
dbus:		root
vcsa:		root
canna:		root
wnn:		root
rpm:		root
nscd:		root
pcap:		root
apache:		root
webalizer:	root
dovecot:	root
fax:		root
quagga:		root
radvd:		root
pvm:		root
amandabackup:		root
privoxy:	root
ident:		root
named:		root
xfs:		root
gdm:		root
mailnull:	root
postgres:	root
sshd:		root
smmsp:		root
postfix:	root
netdump:	root
ldap:		root
squid:		root
ntp:		root
mysql:		root
desktop:	root
rpcuser:	root
rpc:		root
nfsnobody:	root
pcp:		root

ingres:		root
system:		root
toor:		root
manager:	root
dumper:		root
abuse:		abuse

newsadm:	news
newsadmin:	news
usenet:		news
ftpadm:		ftp
ftpadmin:	ftp
ftp-adm:	ftp
ftp-admin:	ftp
www:		webmaster
webmaster:	root
noc:		root
security:	root
hostmaster:	root



# trap decode to catch security attacks
decode:		root

# Person who should get root's mail
#root:		marc

" > /etc/aliases

newaliases

echo " Moving applications / root / scripts 2015 , Configuring crontab == wait ... 
### <=========================================================================================> ###
"

cp /autosmtp/monitoring.sh /root/autosmtp-scripts/monitoring.sh
cp /autosmtp/rbl-2015.info /root/autosmtp-scripts/rbl-2015.info
cp /autosmtp/changeip.sh /root/autosmtp-scripts/changeip.sh




chmod 755 /root/autosmtp-scripts/ -R
cat /tmp/croncria | crontab -
systemctl restart crond
systemctl disable firewalld
systemctl stop firewalld

echo "Cleaning installation please wait ... "

mv /etc/rc.local /etc/rc.local-bkp 
mv /etc/rc.d/rc.local /etc/rc.d/rc.local-bkp
rm -rf /autosmtp

echo '#!/bin/bash

/usr/sbin/ntpdate -u pool.ntp.br >> /dev/null 2>&1 || /usr/bin/rdate -s rdate.cpanel.net >> /dev/null 2>&1

echo "nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
" > /etc/resolv.conf

Versao=`cat /etc/redhat-release`
Domain=`cat /root/autosmtp-scripts/domain.info`
Ip=`head -1 /root/autosmtp-scripts/ips.info`
Data=`date`

echo [#DOMAIN#] > /proc/sys/kernel/hostname
hostname [#DOMAIN#]

echo "

	$Versao

	Welcome to [#DOMAIN#]
	IP: $Ip 
	Last reboot: $Data

	Server Installed Automatically Using a Script Created By Rastu

" > /etc/motd

exit 0 '> /etc/rc.local

ln -s /etc/rc.local /etc/rc.d/rc.local

chmod +x /etc/rc.local 
chmod +x /etc/rc.d/rc.local

echo '
### <=========================================================================================> ###

### ---> Maintenance aliases: "mainservice-[status|stop|start|restart]"
### ---> PMTA monitor aliases: "mainservice-monitor-[stop|start]"

alias mainservice-status="service crond status; service mysqld status; service httpd status; service dovecot status; service postfix status; service pmta status; service pmtahttp status; service named status"
alias mainservice-stop="service crond stop; service mysqld stop; service httpd stop; service dovecot stop; service postfix stop; service pmta stop; service named stop; service pmtahttp stop"
alias mainservice-start="service crond start; service mysqld start; service httpd start; service dovecot start; service postfix start; service pmta start; service named start"
alias mainservice-restart="service crond restart; service mysqld restart; service httpd restart; service dovecot restart; service postfix restart; service pmta restart; service named restart"
alias mainservice-monitor-start="service pmtahttp start"
alias mainservice-monitor-stop="service pmtahttp stop"

### <=========================================================================================> ###' >> /root/.bashrc

systemctl stop iptables >> /dev/null 2>&1
systemctl stop ip6tables >> /dev/null 2>&1
systemctl disable iptables >> /dev/null 2>&1
systemctl disable ip6tables >> /dev/null 2>&1

echo "
### <==================================================================> ###
### ---> This is the server configuration data Created by PmtaExpert.com ==> ###
### <==================================================================> ###



---> SMTP data
     - Email address: pmtadm@[#DOMAIN#]
     - User: pmtadm
     - Password: pmtadm@123

	 - Receiving via IMAP: 
 	 - Secure connection = STARTTLS / Port: 143

     - Sending via SMTP:
     - Insecure Connection / Port: 2525
     
     - Webmail Admin: http://[#DOMAIN#]/mail/?admin
     - User: admin
     - Password: 12345

     - Webmail address: http://[#DOMAIN#]/mail
     - User: pmtadm@[#DOMAIN#]
     - Password: pmtadm@123 

     - Reply Email Address
	 - User: reply
	 - Pass: [#XTRAEMAILPASS#]	 

     - Return (Bounce Email)
     - User: return
     - Pass: [#XTRAEMAILPASS#] 
	 
	 - FBL (Feedback Loop)
	 - User: fbl
	 - Password: [#XTRAEMAILPASS#]
	 
	 - Abuse (Complaints)
	 - User: abuse
	 - Password: [#XTRAEMAILPASS#]
	 
	  Postmaster (Complaints)
	 - User: postmaster
	 - Password: [#XTRAEMAILPASS#]

---> Monitoring data will be sent to:
     - E-mail: [#MONITORINGEMAIL#]

---> SSH Connection
	 - Users access the SSH on port $SSHport
	 - The pmtadm has access to SSH with password pmtadm@123

### <=========================================================================================> ###

---> Server information and PMTA useful commands

	The sending limit per hour of this server is 3200

	To stop powerMTA    -  \"service pmta stop\"
	To start powerMTA   -  \"service pmta start\"
	To restart PowerMTA -  \"service pmta restart\"
	
	------------------------------------------------
	To stop powerMTA management console    -  \"service pmtahttp stop\"
	To start powerMTA management console   -  \"service pmtahttp start\"
	To restart PowerMTA management console -  \"service pmtahttp restart\"
	
	-------------------------------------------------
	
	To debug your powerMTA installation to find errors - \"pmtad --debug\"

### <=========================================================================================> ### 

Reverse DNS Information
------------------------

This is an example of how you should set your Reverse DNS or pass it to your server company, you will find all
of your Reverse DNS entries within your DNS settings below:

Example: mta0.domain.com POINTS TO ===> 100.100.100.1

	
### <=========================================================================================> ### 
" >> /root/autosmtp-scripts/Readme.info

echo "DNS information : " >> /root/autosmtp-scripts/Readme.info
cat /var/named/$ArqNamed >> /root/autosmtp-scripts/Readme.info
cat /root/autosmtp-scripts/Readme.info




echo "
Script Installed = = Success!
Wait for the restart and point the domain as per the instructions above. " 

rm -rf /etc/profile.d/pmta7.sh

shutdown -r 0
